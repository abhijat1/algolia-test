import algoliasearch from 'algoliasearch/lite';
import { InstantSearch, SearchBox, Hits,NumericMenu } from 'react-instantsearch-dom';

const searchClient = algoliasearch('N06AJJQJ1S', '1c5f33b552df644686b77fcf7bc307b4');
const Hit = ({ hit }) => <p className='tooltip'>{hit.title} <span class="tooltiptext">{hit.ingredient.join(", ")}</span></p>;

const App = () => (

  <>
  <link
   rel="stylesheet"
   href="https://cdn.jsdelivr.net/npm/instantsearch.css@8.0.0/themes/satellite-min.css"
   integrity="sha256-p/rGN4RGy6EDumyxF9t7LKxWGg6/MZfGhJM/asKkqvA="
   crossorigin="anonymous"
 />


  <div  className="ais-InstantSearch">
        <InstantSearch indexName="test_data" searchClient={searchClient}>

            
            <div style={{"display":"flex", "justifyContent":"space-around", alignContent:"stretch"}}>
            <div style={{flex:1,paddingLeft:1+"px"}} ><SearchBox autoFocus />
<Hits hitComponent={Hit} /></div>
              <div style={{maxWidth:150+"px", minWidth:150+"px", padding:20+"px"}}>
                <h4>Calorie Count</h4>
                <NumericMenu 
            attribute="cal-count"
           /* add this attribute for faceting */
            items={[
              { label: '<= 100', end: 100 },
              { label: '100 - 200', start: 100, end: 200 },
              { label: '200 - 300', start: 200, end: 300 },
              { label: '300 - 400', start: 300, end: 400 },
              { label: '400 - 500', start: 400, end: 500 },
              { label: '>= 500', start: 500 },
            ]}
/></div>

            
            </div>
        </InstantSearch>
        
      </div>
    </>
);


export default App;